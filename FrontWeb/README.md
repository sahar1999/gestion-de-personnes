# Gestion de Personnes - Partie Web

La partie web de l'application Gestion de Personnes est développée avec ReactJS. Elle permet la gestion des informations relatives aux personnes via une interface utilisateur conviviale.

## Technologies Utilisées

- Reactjs
- Redux

## Installation


1. **Accédez au répertoire du projet :**

    ```bash
    cd FrontWeb
    ```

2. **Installez les dépendances :**

    ```bash
    npm install
    # ou
    yarn install
    ```
3. **Mettre à jour le fichier api.ts :**

Ouvrez le fichier "gestion-de-personnes\FrontWeb\src\features\user\userSlice.ts" dans votre éditeur de texte préféré. Recherchez la section suivante et collez le résultat de @IP :
  
    const url = 'http://<résultat>:5000';

## Utilisation

1. **Lancez l'application en mode développement :**

    ```bash
    npm start
    # ou
    yarn start
    ```

2. **Ouvrez votre navigateur et accédez à l'URL suivante :**

    ```
    http://localhost:3000
    ```

## Fonctionnalités

- Ajout d'utilisateurs
- Affichage de la liste des utilisateurs


