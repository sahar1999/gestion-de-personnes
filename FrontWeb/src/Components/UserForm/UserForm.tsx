import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { addUserAsync } from '../../features/user/userSlice';
import { User } from '../../types';
import './style.css';
import { Dispatch } from 'redux';
import FormInput from './FormInput';
import { Button } from '@mui/material';

const UserForm: React.FC = () => {
  const [nom, setNom] = useState('');
  const [prenom, setPrenom] = useState('');
  const [sexe, setSexe] = useState('');
  const [dateNaissance, setDateNaissance] = useState('');
  const navigate = useNavigate();
  const dispatch: Dispatch<any> = useDispatch();

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    const newUser: User = {
      Nom: nom,
      Prenom: prenom,
      Sexe: sexe,
      DateNaissance: dateNaissance,
    };
    await dispatch(addUserAsync(newUser));
    navigate('/');
  };

  return (
    <div className="conteneur-formulaire-utilisateur">
      <h2>Ajouter un nouvel utilisateur</h2>
      <form onSubmit={handleSubmit} className="formulaire-utilisateur">
        <FormInput label="Nom" value={nom} onChange={(e) => setNom(e.target.value)} required />
        <FormInput label="Prénom" value={prenom} onChange={(e) => setPrenom(e.target.value)} required />
        <FormInput
          label="Sexe"
          value={sexe}
          onChange={(e) => setSexe(e.target.value)}
          isRadio
          options={[
            { label: 'Homme', value: 'M' },
            { label: 'Femme', value: 'F' },
          ]}
          required
        />
        <FormInput label="Date de Naissance" type="date" value={dateNaissance} onChange={(e) => setDateNaissance(e.target.value)} required />
        <div className="button-container">
          <Button variant="outlined" className='List button-space'>
            <Link to="/">Liste des utilisateurs</Link>
          </Button>
          <Button variant="outlined" type="submit" className='Add'>
            Ajouter un utilisateur
          </Button>
        </div>
      </form>
    </div>
  );
};

export default UserForm;
