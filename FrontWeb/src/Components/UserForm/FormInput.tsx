import { FormControl, FormControlLabel, FormLabel, Radio, RadioGroup, TextField } from "@mui/material";
import React from "react";

interface FormInputProps {
  label: string;
  value: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  type?: string;
  required?: boolean;
  isRadio?: boolean;
  options?: { label: string; value: string }[];
}

const FormInput: React.FC<FormInputProps> = ({
  label,
  value,
  onChange,
  type = "text",
  required = false,
  isRadio = false,
  options = [],
}) => {
  return (
    <div className="groupe-formulaire">
      {isRadio ? (
        <FormControl component="fieldset" required={required}>
          <FormLabel>{label} :</FormLabel>
          <RadioGroup
            aria-label={label}
            name={label.toLowerCase()}
            value={value}
            onChange={onChange}
          >
            {options.map((option, index) => (
              <FormControlLabel
                key={index}
                control={<Radio />}
                value={option.value}
                label={option.label}
              />
            ))}
          </RadioGroup>
        </FormControl>
      ) : (
        <TextField
          type={type}
          value={value}
          onChange={onChange}
          className="champ-formulaire"
          required={required}
          InputLabelProps={{
            shrink: true,
          }}
          label={label}
          variant="outlined"
          fullWidth
        />
      )}
    </div>
  );
};

export default FormInput;
