import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { AppDispatch, RootState } from "../../app/store";
import { fetchUsers } from "../../features/user/userSlice";
import { User } from "../../types";
import "./userList.css"; // Import CSS file
import Button from "@mui/material/Button";

const UserList: React.FC = () => {
  const users = useSelector((state: RootState) => state.user.users);
  const status = useSelector((state: RootState) => state.user.status);
  const dispatch: AppDispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchUsers());
  }, [dispatch]);

  if (status === "loading") {
    return <div>Loading...</div>;
  }

  if (status === "failed") {
    return <div>Failed to fetch users.</div>;
  }
  const getGenderText = (gender: string): string => {
    return gender === "M" ? "Homme" : "Femme";
  };


  return (
    <div className="UserList">
      <h2>Liste des Utilisateurs</h2>
      <div className="button-container">
        <Button variant="outlined">
          <Link to="/add-user">Ajouter un utilisateur</Link>
        </Button>
      </div>
      <table className="user-table">
        <thead>
          <tr>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Sexe</th>
            <th>Date de Naissance</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user: User) => (
            <tr key={user._id}>
              <td>{user.Nom}</td>
              <td>{user.Prenom}</td>
              <td>{getGenderText(user.Sexe)}</td>
              <td> {new Date(user.DateNaissance).toLocaleDateString()}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default UserList;
