import { createSlice, PayloadAction, createAsyncThunk } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";
import { User } from "../../types";
const url='http://192.168.140.13:5000'
//  action to fetch all users
export const fetchUsers = createAsyncThunk("user/fetchUsers", async () => {
  const response = await fetch(`${url}/api/users`);
  if (!response.ok) {
    throw new Error("Failed to fetch users");
  }
  return response.json();
});

//  action to add a new user
export const addUserAsync = createAsyncThunk(
  "user/addUser",

  async (newUser: User) => {
    console.log(newUser);
    const response = await fetch(`${url}/api/users`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newUser),
    });
    if (!response.ok) {
      throw new Error("Failed to add user");
    }
    return response.json();
  }
);

interface UserState {
  users: User[];
  status: "idle" | "loading" | "failed";
}

const initialState: UserState = {
  users: [],
  status: "idle",
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchUsers.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchUsers.fulfilled, (state, action) => {
        state.status = "idle";
        state.users = action.payload;
      })
      .addCase(fetchUsers.rejected, (state) => {
        state.status = "failed";
      })
      .addCase(addUserAsync.fulfilled, (state, action: PayloadAction<User>) => {
        state.users.push(action.payload);
      });
  },
});

export const selectAllUsers = (state: RootState) => state.user.users;
export const selectUserById = (userId: string) => (state: RootState) =>
  state.user.users.find((user) => user._id === userId);

export const userStatus = (state: RootState) => state.user.status;

export default userSlice.reducer;
