import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "./App.css";
import USerForm from "./Components/UserForm/UserForm";
import UserList from "./Components/UserList/UserList";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<UserList />} />
        <Route path="/add-User" element={<USerForm />} />
      </Routes>
    </Router>
  );
}

export default App;
