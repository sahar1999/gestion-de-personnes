# Gestion de Personnes

L'application **Gestion de Personnes** est un système complet permettant la gestion des informations relatives aux personnes. Elle se compose de trois parties : une application mobile développée avec React Native et Expo, une application web développée avec ReactJS, et une API backend développée avec Nest.js pour interagir avec une base de données MongoDB.

## Table des Matières

- [Partie Mobile](#partie-mobile)
  - [Installation](#installation-mobile)
  - [Utilisation](#utilisation-mobile)
  - [Fonctionnalités](#fonctionnalités-mobile)
- [Partie Web](#partie-web)
  - [Technologies Utilisées](#technologies-utilisées-web)
  - [Installation](#installation-web)
  - [Utilisation](#utilisation-web)
  - [Fonctionnalités](#fonctionnalités-web)
- [Partie Serveur](#partie-serveur)
  - [Technologies Utilisées](#technologies-utilisées-serveur)
  - [Installation](#installation-serveur)
  - [Configuration](#configuration-serveur)
  - [Lancement](#lancement-serveur)
  - [Points de Terminaison de l'API](#points-de-terminaison-de-lapi)

## Partie Mobile

La partie mobile de l'application est une application développée avec React Native et Expo.

### Installation Mobile

1. **Accédez au répertoire du projet :**

    ```bash
    cd front-Mobile
    ```

2. **Initialisez le projet avec Expo :**

    ```bash
    expo init
    ```

3. **Installez les dépendances :**

    ```bash
    npm install
    # ou
    yarn install
    ```

4. **Mettre à jour le fichier api.ts :**

    Ouvrez le fichier `gestion-de-personnes\Front-Mobile\src\services\api.ts` dans votre éditeur de texte préféré et collez le résultat de @IP :

    ```typescript
    const response = await fetch('http://<résultat>:5000/api/users');
    ```

### Utilisation Mobile

1. **Lancer l'application :**

    ```bash
    expo start
    # ou
    yarn start
    ```

2. **Lancer sur iOS :**

    ```bash
    npm run ios
    # ou
    yarn ios
    ```

3. **Lancer sur Android :**

    ```bash
    npm run android
    # ou
    yarn android
    ```

### Fonctionnalités Mobile

- Ajout d'utilisateurs
- Affichage de la liste des utilisateurs

## Partie Web

La partie web de l'application est développée avec ReactJS.

### Technologies Utilisées Web

- ReactJS
- Redux

### Installation Web

1. **Accédez au répertoire du projet :**

    ```bash
    cd FrontWeb
    ```

2. **Installez les dépendances :**

    ```bash
    npm install
    # ou
    yarn install
    ```

3. **Mettre à jour le fichier `userSlice.ts` :**

    Ouvrez le fichier `gestion-de-personnes\FrontWeb\src\features\user\userSlice.ts` dans votre éditeur de texte préféré et collez le résultat de @IP :

    ```typescript
    const url = 'http://<résultat>:5000';
    ```

### Utilisation Web

1. **Lancez l'application en mode développement :**

    ```bash
     npm run dev
    # ou
    yarn start
    ```

2. **Ouvrez votre navigateur et accédez à l'URL suivante :**

    ```
    http://localhost:3000
    ```

### Fonctionnalités Web

- Ajout d'utilisateurs
- Affichage de la liste des utilisateurs
- Mise à jour et suppression des utilisateurs

## Partie Serveur

La partie serveur est l'API backend développée avec Nest.js pour gérer les personnes dans le système.

### Technologies Utilisées Serveur

- Nest.js
- MongoDB
- Mongoose

### Installation Serveur

1. **Accédez au répertoire du projet :**

    ```bash
    cd Server
    ```

2. **Installez les dépendances :**

    ```bash
    npm install
    ```

### Configuration Serveur

1. **Créez un fichier `.env` dans le répertoire `Server` et ajoutez les variables suivantes :**

    ```bash
    PORT=5000
    LOCAL_IP_ADDRESS=<votre_LOCAL_IP_ADDRESS>
    DB_URL=<votre_URL_de_connexion_mongodb>
    ```

### Lancement Serveur

1. **Lancer l'application :**

    ```bash
    npm run start
    ```

### Points de Terminaison de l'API

- **POST /api/users** : Ajouter une nouvelle personne à la base de données.
- **GET /api/users** : Récupérer la liste de toutes les personnes depuis la base de données.
- **GET /api/users/:id** : Récupérer une personne par son ID.
- **PUT /api/users/:id** : Mettre à jour les détails d'une personne.
- **DELETE /api/users/:id** : Supprimer une personne par son ID.
