import { Injectable, NotFoundException } from '@nestjs/common';
import { User, UserDocument } from 'src/models/users.models';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { UserDto } from 'src/dto/users.dto';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async Add(body: UserDto): Promise<User> {
    const createdUser = new this.userModel(body);
    return createdUser.save();
  }

  async FindAll(): Promise<User[]> {
    return this.userModel.find().exec();
  }

  async FindOne(id: string): Promise<User> {
    const user = await this.userModel.findById(id).exec();
    if (!user) {
      throw new NotFoundException(`User with ID ${id} not found`);
    }
    return user;
  }

  async Update(id: string, body: UserDto): Promise<User> {
    const updatedUser = await this.userModel.findByIdAndUpdate(id, body, { new: true }).exec();
    if (!updatedUser) {
      throw new NotFoundException(`User with ID ${id} not found`);
    }
    return updatedUser;
  }

  async Delete(id: string): Promise<void> {
    const result = await this.userModel.findByIdAndDelete(id).exec();
    if (!result) {
      throw new NotFoundException(`User with ID ${id} not found`);
    }
  }
}

