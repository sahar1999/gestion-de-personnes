import {
  Controller,
  Get,
  Post,
  Param,
  Put,
  Delete,
  Query,
  Body,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { UserDto } from 'src/dto/users.dto';

@Controller('users')
export class UsersController {
  constructor(private readonly service: UsersService) {}

  @Post()
  Add(@Body() body: UserDto) {
    return this.service.Add(body);
  }
  @Get()
  GetAllUsers() {
    return this.service.FindAll();
  }
  @Get('/:id')
  FindUser(@Param('id') id: string) {
    return this.service.FindOne(id);
  }
  @Put('/:id')
  UpdateUser(@Param('id') id: string, @Body() body: UserDto) {
    return this.service.Update(id, body);
  }
  @Delete('/:id')
  DeleteUser(@Param('id') id: string) {
    return this.service.Delete(id);
  }
}
