import { IsNotEmpty } from 'class-validator';

export class UserDto {
  @IsNotEmpty()
  Nom: string;
  @IsNotEmpty()
  Prenom: string;
  @IsNotEmpty()
  Sexe: string;
  @IsNotEmpty()
  DateNaissance: Date;
}
