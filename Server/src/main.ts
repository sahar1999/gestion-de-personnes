import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationError, ValidationPipe } from '@nestjs/common';
import {
  ValidationException,
  ValidationFilter,
} from './util/filter.validation';
async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();

  app.setGlobalPrefix('/api');
  app.useGlobalFilters(new ValidationFilter());
  app.useGlobalPipes(
    new ValidationPipe({
      skipMissingProperties: false,
      exceptionFactory: (errors: ValidationError[]) => {
        const errMsg = {};
        errors.forEach((err) => {
          errMsg[err.property] = [...Object.values(err.constraints)];
        });
        return new ValidationException(errMsg);
      },
    }),
  );
  const localIPAddress = process.env.LOCAL_IP_ADDRESS || 'localhost';

  const port = process.env.PORT || 5000;
  await app.listen(port, localIPAddress);
  console.log(`NestJS server is listening on http://${localIPAddress}:${port}`);
}
bootstrap();
