import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop({ required: true })
  Nom: string;

  @Prop({ required: true })
  Prenom: string;

  @Prop({ required: true })
  Sexe: string;

  @Prop({
    required: true,
    type: Date,
    set: (date: string) => {
      const [day, month, year] = date.split('/');
      return new Date(`${year}-${month}-${day}`);
    },
  })
  DateNaissance: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);
