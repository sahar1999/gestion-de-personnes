<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

<p align="center">Un framework progressif <a href="http://nodejs.org" target="_blank">Node.js</a> pour construire des applications côté serveur efficaces et évolutives.</p>
<p align="center">
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core?style=flat-square" alt="Version NPM" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core?style=flat-square" alt="Licence du package" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common?style=flat-square" alt="Téléchargements NPM" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master?style=flat-square" alt="CircleCI" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Couverture" /></a>
<a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-en%20ligne-brightgreen.svg?style=flat-square" alt="Discord"/></a>
<a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg?style=flat-square" alt="Supporteurs sur Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg?style=flat-square" alt="Sponsors sur Open Collective" /></a>
<a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg?style=flat-square"/></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://img.shields.io/badge/Supportez%20nous-Open%20Collective-41B883.svg?style=flat-square" alt="Supportez nous"></a>
<a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow&style=flat-square"></a>
</p>

# Partie Serveur - Gestion de Personnes

Ceci est l'API backend pour gérer les personnes dans un système. Elle fournit des points de terminaison pour ajouter et récupérer des personnes depuis une base de données MongoDB en utilisant Nest.js.

## Technologies Utilisées

- Nest.js
- MongoDB
- Mongoose

## Installation

1.  **Accédez au répertoire du projet :**

    ```bash
     cd Server
    ```

2.  **Installez les dépendances :**

    ```bash
     npm install
    ```

3.  **Configurez les variables d'environnement :**

Créez un fichier .env dans le répertoire Server et ajoutez ce qui suit:

PORT=5000

LOCAL_IP_ADDRESS=<votre_LOCAL_IP_ADDRESS>

DB_URL=<votre_URL_de_connexion_mongodb>

Créez un fichier .env dans le répertoire Server et ajoutez ce qui suit:
PORT=5000
DB_URL=<votre_URL_de_connexion_mongodb>

## Lancer l'application

     npm run start

## Points de Terminaison de l'API

- POST /api/users : Ajouter une nouvelle personne à la base de données.

- GET /api/users : Récupérer la liste de toutes les personnes depuis la base de données.

- GET /api/users/:id : Récupérer une personne par son ID.

- PUT /api/users/:id : Mettre à jour les détails d'une personne.

- DELETE /api/users/:id : Supprimer une personne par son ID.
