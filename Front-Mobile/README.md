# Partie Mobile - Gestion de Personnes

La partie mobile de l'application Gestion de Personnes est une application développée avec React Native et Expo. Elle est conçue pour permettre la gestion des informations relatives aux personnes.


## Table des Matières

- [Utilisation](#utilisation)
- [Fonctionnalités](#fonctionnalités)

## Installation

1.  **Accédez au répertoire du projet :**

    ```bash
     cd front-Mobile
    ```

2.  **Initialisez le projet avec Expo :**

    ```bash
     expo init
    ```

3.  **Installer les dépendances :**

    ```bash
    npm install
    # ou
    yarn install
    ```
4. **Mettre à jour le fichier api.ts :**

Ouvrez le fichier gestion-de-personnes\Front-Mobile\src\services\api.ts dans votre éditeur de texte préféré. Recherchez la section suivante et collez le résultat de @IP :
  
    const response = await fetch(http://`<résultat>:5000/api/users`);

## Utilisation

1. **Lancer l'application**

   ```bash
   expo start
   # ou
   yarn start
   ```

2. **Lancer sur iOS**

   ```bash
   npm run ios
   # ou
   yarn ios
   ```

3. **Lancer sur Android**

   ```bash
   npm run android
   # ou
   yarn android
   ```
