import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { User } from "../types/types";

interface TableProps {
  data: User[];
}

const Table: React.FC<TableProps> = ({ data }) => {
  const getGenderText = (gender: string): string => {
    return gender === "M" ? "Homme" : "Femme";
  };

  return (
    <View style={styles.table}>
      <View style={styles.headerRow}>
        <Text style={[styles.cell, styles.cellHeader]}>Prénom</Text>
        <Text style={[styles.cell, styles.cellHeader]}>Nom</Text>
        <Text style={[styles.cell, styles.cellHeader]}>Sexe</Text>
        <Text style={[styles.cell, styles.cellHeader]}>Date de naissance</Text>
      </View>
      {data.map((user, index) => (
        <View key={user._id} style={[styles.row, index % 2 === 0 ? styles.evenRow : styles.oddRow]}>
          <Text style={styles.cell}>{user.Prenom}</Text>
          <Text style={styles.cell}>{user.Nom}</Text>
          <Text style={styles.cell}>{getGenderText(user.Sexe)}</Text>
          <Text style={styles.cell}>
            {new Date(user.DateNaissance).toLocaleDateString()}
          </Text>
        </View>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  table: {
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 10,
    overflow: "hidden",
    marginBottom: 20,
    backgroundColor: "#fff",
  },
  headerRow: {
    flexDirection: "row",
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
    backgroundColor: "#f2f2f2",
  },
  row: {
    flexDirection: "row",
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
  },
  evenRow: {
    backgroundColor: "#f9f9f9",
  },
  oddRow: {
    backgroundColor: "#eaeaea",
  },
  cellHeader: {
    fontWeight: "bold",
    paddingVertical: 12,
    paddingHorizontal: 8,
    textAlign: "center",
  },
  cell: {
    flex: 1,
    paddingVertical: 12,
    paddingHorizontal: 8,
    textAlign: "center",
  },
});

export default Table;
