import React, { useEffect, useState } from "react";
import { View, Text } from "react-native";
import { User } from "../types/types";
import { fetchUsers } from "../services/api";
import Table from "./Table";
import { globalStyles, tableStyles } from "../styles";

const UserList: React.FC = () => {
  const [users, setUsers] = useState<User[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = await fetchUsers();
        setUsers(data);
        setLoading(false);
      } catch (error) {
        console.error(error);
        setError("Erreur lors du chargement des utilisateurs");
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  if (loading) {
    return <Text style={[globalStyles.container, tableStyles.loadingMessage]}>Chargement...</Text>;
  }

  if (error) {
    return <Text style={[globalStyles.container, tableStyles.errorMessage]}>{error}</Text>;
  }

  return (
    <View style={globalStyles.container}>
      <Text style={tableStyles.title}>Liste des Utilisateurs :</Text>
      <Table data={users} />
    </View>
  );
};

export default UserList;
