// types/types.ts
export interface User {
  _id: string;
  Nom: string;
  Prenom: string;
  Sexe: string;
  DateNaissance: string;
}
export interface UserState {
  users: User[];
  loading: boolean;
  error: string | null;
}
