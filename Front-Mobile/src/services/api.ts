// api.ts
import { User } from "../types/types";

export async function fetchUsers(): Promise<User[]> {
  try {
    const response = await fetch("http://192.168.140.13:5000/api/users");
    if (!response.ok) {
      throw new Error("Network response was not ok");
    }
    const data: User[] = await response.json();
    return data;
  } catch (error) {
    console.error(error);
    throw new Error("Failed to fetch users");
  }
}
