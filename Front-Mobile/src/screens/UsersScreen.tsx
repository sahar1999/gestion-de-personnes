// src/screens/UsersScreen.tsx
import React from "react";
import { View } from "react-native";
import UserList from "../components/UserList";

const UsersScreen: React.FC = () => {
  return (
    <View style={{ flex: 1, justifyContent: "center" }}>
      <UserList />
    </View>
  );
};

export default UsersScreen;
